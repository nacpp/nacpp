
#include "complex.h"



namespace NA {

template<typename T>
inline CComplex_<T>::CComplex_()
{
}

template<typename T>
CComplex_<T>::CComplex_(T & real, T & image)
{
	m_real = real;
	m_image = image;
}

template<typename T>
inline CComplex_<T>::~CComplex_()
{
}

template<typename T>
bool CComplex_<T>::operator==(const CComplex_<T>& com)
{
	if ((m_read == com.m_real) && (m_image == com.m_image))
		return true;
	return false;
}

template<typename T>
CComplex_<T>& CComplex_<T>::operator=(const CComplex_<T>& c)
{
	m_real = c.m_real;
	m_image = c.m_image;
	return *this;
}

template<typename T>
CComplex_<T>& CComplex_<T>::operator+(const CComplex_<T>& c)
{
	m_read += c.m_real;
	m_image += c.m_image;
	reurn *this;
}

template<typename T>
CComplex_<T>& CComplex_<T>::operator-(const CComplex_<T>& c)
{
	m_real -= c.m_real;
	m_image -= c.m_image;
	return *this;
}

template<typename T>
CComplex_<T>& CComplex_<T>::operator*(const CComplex_<T>& c)
{
	T real, image;
	real = m_real * c.m_real - m_image * c.m_image;
	image = m_real * c.m_image + m_image * c.m_real;
	m_real = real;
	m_image = image;
	return *this;
}

template<typename T>
CComplex_<T>& CComplex_<T>::operator/(const CComplex_<T>& c)
{
	T real, image;
	T t = c.m_real*c.m_real + c.m_image*c.m_image;
	real = (m_real*c.m_real - m_image * (-c.m_image)) / t;
	image = (m_real*(-c.m_image) + m_image * c.m_real) / t;
	m_real = real;
	m_image = image;
	return *this;

}




}