#include "namath.h"

namespace NA {

double factorial(int n) {
	if (n < 0)
		return -1;

	double m = 1;
	for (int i = 2; i <= n; i++)
	{
		m *= i;
	}
	return m;
}


}

