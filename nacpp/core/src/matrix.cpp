#include "matrix.h"
#include <iostream>

namespace NA {





Matrix::Matrix()
{
	m_data = nullptr;
	m_recorder = nullptr;
	new_s(m_recorder);
	m_recorder->refcount = 1;
	m_recorder->flag = 0;
}

Matrix::Matrix(int row, int col)
	:Matrix()
{
	this->m_rows = row;
	this->m_cols = col;
	size_t len = row * col;
	newarray_s(m_data, len);
}

Matrix::Matrix(Matrix & mat)
{
	*this = mat;
}

Matrix::Matrix(const MatrixEx & mate)
{
	m_rows = mate.m_rows;
	m_cols = mate.m_cols;
	m_data = mate.m_data;
	m_recorder = nullptr;
	new_s(m_recorder);
	m_recorder->refcount = 1;
}

Matrix::~Matrix()
{
	clear();
}

Matrix  Matrix::operator=( Matrix & mat)
{
	if (this->m_data != mat.ptr()) {
		this->clear();
	}
	this->m_data = mat.ptr();
	this->m_recorder = mat.refCountPtr();
	(m_recorder->refcount)++;
	return *this;
}

Matrix& Matrix::operator=(const  MatrixEx & mate)
{
	this->clear();
	new_s(this->m_recorder);
	mate.assign(*this);
	this->m_recorder->refcount = 1;
	return *this;
}


int Matrix::setTo(const double & value)
{
	for (int i = 0; i < m_rows; i++) {
		for (int j = 0; j < m_cols; j++) {
			m_data[i*m_cols + j] = value;
		}
	}

	return 0;
}

Matrix  Matrix::zeros(int m_rows, int m_cols)
{
	Matrix mat(m_rows, m_cols);
	mat.setTo(0.0);
	return mat;
}

MatrixEx  Matrix::eye(int dims)
{
	MatrixEx e(dims,dims);
	double * data = e.ptr();
	for (int i = 0; i < dims; i++) {
		for (int j = 0; j < dims; j++) {
			if (i == j)
				data[i*dims + j] = 1.0;
			else
				data[i*dims + j] = 0.0;
		}
	}
	return e;
}

MatrixEx& Matrix::add(Matrix & lmat, Matrix & rmat)
{
	int m_rows = lmat.m_rows;
	int m_cols = lmat.m_cols;
	NA_Assert(m_rows == rmat.m_rows);
	NA_Assert(m_cols == rmat.m_cols);
	MatrixEx ret(m_rows, m_cols);
	double * data = ret.ptr();
	for (int i = 0; i < m_rows; i++) {
		for (int j = 0; j < m_cols; j++) {
			data[i*m_cols + j] = lmat.m_data[i*m_cols + j] + rmat.m_data[i*m_cols + j];
		}
	}
	return ret;
}

void Matrix::show()
{
	std::cout << "m_rows:" << m_rows << " " << "m_cols:" << m_cols << std::endl;
	for (int i = 0; i < m_rows; i++) {
		for (int j = 0; j < m_cols; j++) {
				std::cout << m_data[i* m_cols + j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}





double * Matrix::ptr()
{
	return m_data;
}

CRecorder * Matrix::refCountPtr()
{
	return m_recorder;
}

void Matrix::clear()
{
	(m_recorder->refcount)--;
	if (0 == (m_recorder->refcount)) {
		delete_s(m_data);
		delete_s(m_recorder);
	}
}



MatrixEx::MatrixEx()
{
}

MatrixEx::MatrixEx(int rows, int cols)
{
	m_rows = rows;
	m_cols = cols;
	m_data = new double[rows*cols];
}

MatrixEx::MatrixEx(const Matrix & mat)
{
	m_rows = mat.m_rows;
	m_cols = mat.m_cols;
	m_data = mat.m_data;
}

MatrixEx::operator Matrix() const
{
	Matrix m;
	this->assign( m);
	return m;
}

void MatrixEx::assign(Matrix & mat) const 
{
	mat.m_rows = m_rows;
	mat.m_cols = m_cols;
	mat.m_data = m_data;
}

double * MatrixEx::ptr()
{
	return m_data;
}

}



