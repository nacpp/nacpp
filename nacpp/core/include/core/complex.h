/*M///////////////////////////////////////////////////////////////////////////////////////
//author: cclplus
//date : 2020/09/06
//实现复数类
//note::复数类的使用不再需要引用计数器，因此返回值该用返回引用的方式
//M*/
#pragma once

#include "core_global.h"


namespace NA
{
template <typename T>
class DLL_EXPORT CComplex_
{
public:
	CComplex_();
	CComplex_(T& real, T & image);
	~CComplex_();
	//!<运算符重载
	bool operator==(const CComplex_<T> & com);
	CComplex_<T>& operator=(const CComplex_<T> &c);
	CComplex_<T>& operator+(const CComplex_<T> &c);
	CComplex_<T>& operator-(const CComplex_<T> &c);
	CComplex_<T>& operator*(const CComplex_<T> &c);
	CComplex_<T>& operator/(const CComplex_<T> &c);
private:
	T m_real;
	T m_image;

};




}

