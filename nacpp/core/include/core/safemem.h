
/*M///////////////////////////////////////////////////////////////////////////////////////
//author: cclplus
//date : 2020/09/05
//实现内存的安全申请与释放，内存问题是C++代码崩溃的最常见问题
//M*/
#pragma once
#include "type.h"
namespace NA {
template <typename T>
int new_s(T * &p) {
	if (nullptr == p) {
		p = new T;
		return RET_OK;
	}

	return ERR_REPNEW;
}

template <typename T>
int newarray_s(T * &p,const size_t & size) {
	if (nullptr == p) {
		p = new T[size];
		return RET_OK;
	}

	return ERR_REPNEW;
}

template <typename T>
int delete_s(T * &p) {
	if (nullptr == p) {
		return ERR_REPDEL;
	}
	delete p;
	p = nullptr;
	return RET_OK;
}

//!<安全清楚数组
template <typename T>
int deletearray_s(T * &p) {
	if (nullptr == p) {
		return ERR_REPDEL;
	}
	delete[] p;
	p = nullptr;
	return RET_OK;
}


}
