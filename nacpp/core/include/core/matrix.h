/*M///////////////////////////////////////////////////////////////////////////////////////
//author: cclplus
//date : 2020/09/05
//实现矩阵类，通过引用计数器进行析构
//M*/
#pragma once
#include "core_global.h"
namespace NA {

struct	CRecorder{
	int refcount;
	int flag;//!<flag 为0时表示计算数值有效，其余表示计算无效
};
class  MatrixEx;
class DLL_EXPORT Matrix 
{
public:
	Matrix();
	Matrix(int row,int col);
	//!<复制构造函数(注:不复制data的内容)
	Matrix(Matrix & mat);
	Matrix(const MatrixEx & mate);
	virtual ~Matrix();
	
	//!<预算符重载
	Matrix operator=(Matrix& mat);
	
	Matrix& operator=(const MatrixEx & mate);
	//!<设置矩阵的值
	int setTo(const double & value );


	//!<获取data部分的指针
	double * ptr();
	//!<获取引用计数器的指针
	CRecorder * refCountPtr();
	//!<静态函数
	static Matrix zeros(int rows, int cols);
	static MatrixEx eye(int dims);

	static MatrixEx& add(Matrix & lmat,Matrix & rmat);

	void show();


	int  m_rows;
	int  m_cols;
	void clear();
	double * m_data;
	CRecorder * m_recorder;
	

	
	

};

//!<不会析构内容的MatrixEx
class DLL_EXPORT MatrixEx 
{
public:
	MatrixEx();
	MatrixEx(int rows,int cols);
	explicit MatrixEx(const Matrix & mat);
	operator Matrix() const;

	void assign(Matrix & mat) const;
	double * ptr();

	int  m_rows;
	int  m_cols;
	double * m_data;
};


}





