
#include "zernike.h"
#include <cmath>
namespace NA {
void getNoll(const int   j,
	int & n,
	int & m)
{
	if (j == 0)
	{
		n = m = 0;
		return;
	}

	int r = 1;
	while (j > (r*(r + 1)) / 2)
		r++;

	n = r - 1;
	m = r - 1 - 2 * (r*(r + 1) / 2 - j);
}




/*
double ZernikePolynomial(unsigned int n,
	int          m,
	double       r,
	double       theta)
{
	unsigned int m_abs = std::abs(m);
	double normalization = std::sqrt((2.0*n + 2.0) / ((1.0 + (m == 0 ? 1.0 : 0.0))));
	double radial_poly;// = ZernikeRadialPolynomial(n, m_abs, r, recursion);
	double angular_poly = (m < 0 ? std::sin(m_abs*theta) : std::cos(m_abs*theta));

	return normalization * radial_poly*angular_poly;
}
*/

}
