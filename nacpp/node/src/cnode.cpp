#include "cnode.h"


namespace NA {



CNode::CNode()
{
}

CNode::CNode(std::string name)
{
	m_name = name;
}

CNode::CNode(std::string name, CNode * parent)
	:CNode(name)
{
	m_parent = parent;
	m_parent->m_childs.push_back(this);
}

CNode::~CNode()
{
}

int CNode::load()
{
	return 0;
}

int CNode::save()
{
	return 0;
}

CNode * CNode::getParent()
{
	return m_parent;
}

std::vector<CNode*> CNode::getChilds()
{
	return m_childs;
}

}
