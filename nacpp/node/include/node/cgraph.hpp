/*M///////////////////////////////////////////////////////////////////////////////////////
//author: cclplus
//date : 2020/09/09
//实现图等结构，主要用于DFS和BFS
//M*/
#pragma once
#include <vector>
namespace NA {
/*
 *实现二叉树的结点
 */
template<typename T>
class CBinTreeNode 
{
public:
	CBinTreeNode() {
		m_left = nullptr;
		m_right = nullptr;
	}
	CBinTreeNode(const T & data)
		:CBinTreeNode()
	{
		m_data = data;
	}
	~CBinTreeNode(){

	}

	T m_data;
	CBinTreeNode * m_parent;//!<父结点
	CBinTreeNode * m_left;//!<左子树
	CBinTreeNode * m_right;//!<右子树
};

/*
 *二叉树类
 */
template<typename T>
class CBinTree 
{
	typedef BinTreeNode<T> * Node;
public:
	CBinTree() {
		m_root = nullptr;
	}

	~CBinTree() {
		std::vector<Node> nList = this->DFS();
		for (auto iter = nList.begin(); iter != nList.end(); iter++)
			delete *iter;
	}

	void setRoot(Node  root) {
		m_root = root;
	}
	

	//!<实现二叉树的广度优先搜索算法
	std::vector<Node> BFS() {

		return std::vector<Node>();
	}
	//!<实现二叉树的深度优先搜索算法
	std::vector<Node> DFS() {

		return std::vector<Node>();
	}
private:
	Node  m_root;



};


/*
 *树的节点
 */
template<typename T>
class CTreeNode 
{
	typename CTreeNode<T>  *   Node;
public:
	CTreeNode() {
		m_parent = nullptr;
	}
	CTreeNode(const Node & parent)
		:CTreeNode()
	{
		m_parent = parent;
	}
	~CTreeNode() {

	}
	std::vector<Node> getChilds() {
		return m_childs;
	}
	void appendChild(const Node & child ) {
		m_childs.push_back(child);
	}

private:
	std::vector<Node> m_childs;
	Node			  m_parent;

};
//!<实现树
template<typename T>
class CTree
{
	typename CTreeNode<T>  *   Node;
public:
	CTree() {

	}
	~CTree() {

	}
	void setRoot(Node root) {
		m_root = root;
	}

	//!<实现树的广度优先搜索算法
	std::vector<Node> BFS() 
	{
		std::vector<Node> bfslist;
		bfslist.push_back(m_root);
		std::vector<Node> child;
		child = m_root->getChilds();

		std::vector<Node> nchild;
		bool flag = true;
		while (flag) {
			nchild.clear();
			for (auto iter = child.begin(); iter != child.end(); iter++) {
				nchild += (*iter)->getChilds();
			}
			if (nchild.empty()) {
				flag = false;
			}
			else {
				bfslist += nchild;
				child = nchild;
			}
		}
		
		return bfslist;
	}
	//!<实现树的深度优先搜索算法
	std::vector<Node> DFS() 
	{

		return std::vector<Node>();
	}
private:
	Node m_root;


};








}








