/*M///////////////////////////////////////////////////////////////////////////////////////
//author: cclplus
//date : 2020/09/05
//用于实现参数管理，以CNode为结点，保存全部参数
//M*/
#pragma once
#include <core.hpp>
#include <string>
#include <vector>
namespace NA {
class DLL_EXPORT CNode
{
public:
	CNode();
	CNode(std::string name);
	CNode(std::string name, CNode * parent);
	virtual ~CNode();
	//!<参数加载
	virtual int load();
	//!<参数读取
	virtual int save();

	//!<获取父结点
	CNode * getParent();
	//!<获取全部子结点
	std::vector<CNode*> getChilds();

private:
	std::string m_name;
	CNode * m_parent;//!<父结点
	std::vector<CNode*> m_childs;
	//load & save data


};




}



